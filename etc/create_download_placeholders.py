#!/usr/bin/env python3
"""
Usage: create_download_placeholders.py

Run this script to create a set of placeholder build files in the 'download'
directory inside 'webroot'. This helps to make the website feel alive with
content when developing locally.
"""

import pathlib

dir_download = pathlib.Path(__file__).parent.parent.absolute() / 'webroot' / 'download'

builds = {
    'daily': [
        'blender-4.3.0-alpha+main.8da3b74ee2a6-darwin.x86_64-release.dmg',
        'blender-4.3.0-alpha+main.8da3b74ee2a6-darwin.x86_64-release.dmg.sha256',
        'blender-4.3.0-alpha+main.8da3b74ee2a6-darwin.arm64-release.dmg',
        'blender-4.3.0-alpha+main.8da3b74ee2a6-darwin.arm64-release.dmg.sha256',
        'blender-4.2.0-beta+v42.4e4d8476c49f-darwin.x86_64-release.dmg',
        'blender-4.2.0-beta+v42.4e4d8476c49f-darwin.arm64-release.dmg',
        'blender-3.6.13-stable+v36.791bdfd03f07-darwin.arm64-release.dmg',
        'blender-3.6.13-stable+v36.791bdfd03f07-darwin.x86_64-release.dmg',
        'blender-3.3.20-stable+v33.969bd15cc419-darwin.x86_64-release.dmg',
        'blender-3.3.20-stable+v33.969bd15cc419-darwin.arm64-release.dmg',

        'blender-4.3.0-alpha+main.8da3b74ee2a6-windows.arm64-release.zip',
        'blender-4.3.0-alpha+main.8da3b74ee2a6-windows.arm64-release.zip.sha256',
        'blender-4.3.0-alpha+main.8da3b74ee2a6-windows.amd64-release.zip',
        'blender-4.3.0-alpha+main.8da3b74ee2a6-windows.amd64-release.zip.sha256',
        'blender-4.2.0-beta+v42.4e4d8476c49f-windows.amd64-release.zip',
        'blender-3.6.13-stable+v36.791bdfd03f07-windows.amd64-release.msi',
        'blender-3.6.13-stable+v36.791bdfd03f07-windows.amd64-release.msi.sha256',
        'blender-3.6.13-stable+v36.791bdfd03f07-windows.amd64-release.zip',
        'blender-3.6.13-stable+v36.791bdfd03f07-windows.amd64-release.msix',
        'blender-3.6.13-stable+v36.791bdfd03f07-windows.amd64-release.msix.sha256',
        'blender-3.3.20-stable+v33.969bd15cc419-windows.amd64-release.msi',
        'blender-3.3.20-stable+v33.969bd15cc419-windows.amd64-release.zip',
        'blender-3.3.20-stable+v33.969bd15cc419-windows.amd64-release.msix',

        'blender-4.3.0-alpha+main.8da3b74ee2a6-linux.x86_64-release.tar.xz',
        'blender-4.3.0-alpha+main.8da3b74ee2a6-linux.x86_64-release.tar.xz.sha256',
        'blender-4.2.0-beta+v42.4e4d8476c49f-linux.x86_64-release.tar.xz',
        'blender-3.6.13-stable+v36.791bdfd03f07-linux.x86_64-release.tar.xz',
        'blender-3.3.20-stable+v33.969bd15cc419-linux.x86_64-release.tar.xz',

        'blender-3.1.1-candidate+v31.fc0a07da29ed-darwin.x86_64-release.dmg',
        'blender-3.1.1-candidate+v31.fc0a07da29ed-darwin.arm64-release.dmg',
        'blender-3.1.1-candidate+v31.fc0a07da29ed-windows.amd64-release.zip',
        'blender-3.1.1-candidate+v31.fc0a07da29ed-windows.arm64-release.zip',
        'blender-3.1.1-candidate+v31.fc0a07da29ed-linux.x86_64-release.tar.xz',
    ],
    'experimental': [
        'blender-4.3.0-alpha+brush-assets-project.4ed140a4db7a-darwin.x86_64-release.dmg',
        'blender-4.3.0-alpha+brush-assets-project.4ed140a4db7a-darwin.arm64-release.dmg',

        'blender-4.2.0-alpha+universal-scene-description.6fcf7fd12a76-darwin.arm64-release.dmg',
        'blender-4.2.0-alpha+universal-scene-description.6fcf7fd12a76-darwin.x86_64-release.dmg',

        'blender-4.3.0-alpha+brush-assets-project.4ed140a4db7a-windows.arm64-release.zip',
        'blender-4.3.0-alpha+brush-assets-project.4ed140a4db7a-windows.amd64-release.zip',
        'blender-4.2.0-alpha+universal-scene-description.6fcf7fd12a76-windows.amd64-release.zip',

        'blender-4.3.0-alpha+brush-assets-project.4ed140a4db7a-linux.x86_64-release.tar.xz',
        'blender-4.2.0-alpha+universal-scene-description.6fcf7fd12a76-linux.x86_64-release.tar.xz',

        'blender-3.2.0-alpha+temp-sculpt-colors.51c3ce1353db-darwin.x86_64-release.dmg',
        'blender-3.2.0-alpha+temp-3d-texturing-brush-b.94d7db2cc002-darwin.x86_64-release.dmg',
        'blender-3.2.0-alpha+temp-3d-texturing-brush-b.94d7db2cc002-windows.amd64-release.zip',
        'blender-3.2.0-alpha+xr-dev.ff347bf95023-windows.amd64-release.zip',
        'blender-3.2.0-alpha+xr-dev.ff347bf95023-linux.x86_64-release.tar.xz',
        'blender-3.2.0-alpha+sculpt-dev.891b8797ed0a-linux.x86_64-release.tar.xz',
    ],
    'patch': [
        'blender-4.3.0-alpha+main-PR122834.9cc48bb549db-windows.amd64-release.zip',
        'blender-4.2.0-beta+main-PR122807.33a7750acdf6-windows.amd64-release.zip',
        'blender-4.1.1-candidate+main-PR120149.85bdfb8cddcd-windows.amd64-release.zip',
        'blender-3.3.18-stable+main-PR120774.6820bfe3fbb8-windows.amd64-release.zip',
        'blender-4.3.0-alpha+main-PR122834.9cc48bb549db-windows.arm64-release.zip',
        'blender-4.2.0-beta+main-PR122807.33a7750acdf6-windows.arm64-release.zip',
        'blender-4.1.1-candidate+main-PR120149.85bdfb8cddcd-windows.arm64-release.zip',
        'blender-3.3.18-stable+main-PR120774.6820bfe3fbb8-windows.arm64-release.zip',

        'blender-4.3.0-alpha+main-PR122834.9cc48bb549db-darwin.x86_64-release.zip',
        'blender-4.2.0-beta+main-PR122807.33a7750acdf6-darwin.x86_64-release.zip',
        'blender-4.1.1-candidate+main-PR120149.85bdfb8cddcd-darwin.x86_64-release.zip',
        'blender-3.3.18-stable+main-PR120774.6820bfe3fbb8-darwin.x86_64-release.zip',
        'blender-4.3.0-alpha+main-PR122834.9cc48bb549db-darwin.arm64-release.zip',
        'blender-4.2.0-beta+main-PR122807.33a7750acdf6-darwin.arm64-release.zip',
        'blender-4.1.1-candidate+main-PR120149.85bdfb8cddcd-darwin.arm64-release.zip',
        'blender-3.3.18-stable+main-PR120774.6820bfe3fbb8-darwin.arm64-release.zip',

        'blender-4.3.0-alpha+main-PR122834.9cc48bb549db-linux.x86_64-release.zip',
        'blender-4.2.0-beta+main-PR122807.33a7750acdf6-linux.x86_64-release.zip',
        'blender-4.1.1-candidate+main-PR120149.85bdfb8cddcd-linux.x86_64-release.zip',
        'blender-3.3.18-stable+main-PR120774.6820bfe3fbb8-linux.x86_64-release.zip',
    ]
}

for build_type, build_names in builds.items():
    dir_build_type = dir_download / build_type
    pathlib.Path(dir_build_type).mkdir(parents=True, exist_ok=True)
    for build_name in build_names:
        pathlib.Path(dir_build_type / build_name).touch()
