# Blender Buildbot WWW

This repository contains the sources for the builder.blender.org website.

## Development setup

System requirements:
* Apache
* PHP 7.1+

To run the site:
* `a2enmod proxy`, `a2enmod rewrite` and `a2enmod headers` to enable proxy pass, rewrite rules and setting headers
* Add the `000-default.conf` to the `sites-available`

### Setup with Docker

To run the website using Docker, follow these steps:

Use the provided `Dockerfile` and `docker-compose.yml` to build and start the containers. Execute the following command:
```sh
docker compose up --build
# --build makes sure to rebuild the image in case there were changes to the Dockerfile
```

Once the containers are up and running, you can access the website on port `8080`:
```sh
http://localhost:8080
```

If you prefer to use a different port, you can override the default port by setting the `HOST_PORT` environment variable. For example, to use port `9090`, run:
```sh
HOST_PORT=9090 docker compose up
```
Then, access the website at:
```sh
http://localhost:9090
```

## The download directory

For the site to run as expected it's necessary to create a directories and files structure similar to this:

```
└── download
     └── daily
     │   └── [daily builds]
     └── patch
     │   └── [builds from patches]
     └── experimental
         └── [experimental builds, from branches]
```

Use the `etc/create_download_placeholders.py` script to create some placeholders.
