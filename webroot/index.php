<?php

function main() {
  handleRequest();
}

$root = dirname(dirname(__FILE__));
require_once $root . '/source/main.php';
require_once $root . '/source/util.php';

main();

?>
