FROM php:8.1-apache

RUN a2enmod proxy \
    && a2enmod rewrite \
    && a2enmod headers \
    && a2enmod proxy_wstunnel \
    && a2enmod proxy_http

EXPOSE 80