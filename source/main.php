<?php

require_once 'Build.php';
require_once 'BuildsDirectoryLister.php';
require_once 'BuildsRenderer.php';
require_once 'action.php';
require_once 'rules.php';
require_once 'util.php';


function handleNotFoundRequest() {
  http_response_code(404);
}

function renderDownloadResponseAsJSON($lister) {
  header('Content-Type: application/json; charset=utf-8');
  // Require v=1 or v=2 to be specified
  if (!isset($_GET['v']) || ($_GET['v'] != '1' && $_GET['v'] != '2')) {
    $data = ['error' => 'Invalid version specified. Please provide a supported version using v=1 or v=2.'];
    echo json_encode( $data );
    http_response_code(400);
    return;
  }
  $builds = $lister->getBuilds();
  $renderer = new BuildsRenderer($builds);
  $renderer->renderJSON($_GET['v']);
}

function renderDownloadResponseAsHTML($lister) {
  // Serve the directory listing as HTML.
  require 'templates/header.php';
  // Logic inside content_build_lister.php will render the list of builds
  require 'templates/content_build_lister.php';
  require 'templates/footer.php';
}

function handleDownloadRequest() {
  $lister = createListerForCurrentRequest();
  $is_valid = $lister->isValid();

  // Requested path is valid, but folder is not found.
  if ($is_valid && !$lister->exists()) {
    http_response_code(404);
    return;
  }

  // Invalid paths go to a pre-defined location.
  if (!$is_valid) {
    header('Location: /download/daily/');
    return;
  }

  // Check if we are requesting a JSON formatted view.
  if (isset($_GET['format']) && $_GET['format'] == 'json') {
    renderDownloadResponseAsJSON($lister);
    return;
  }

  renderDownloadResponseAsHTML($lister);

}

function handleDashboardRequest() {
  require 'templates/header.php';
  require 'templates/content_dashboard.php';
  require 'templates/footer.php';
}

function handleTestsRequest() {
  require 'templates/header.php';
  require 'templates/content_tests.php';
  require 'templates/footer.php';
}

function getRedirectLocationFromURL(array $url) {
  $result = $url['path'];
  if (substr($url['path'], -1) != '/') {
    $result .= '/';
  }
  if (isset($url['query'])) {
    $result .= '?' . $url['query'];
  }
  return $result;
}

// Router
function handleRequest() {
  if (handleSpecialAction()) {
    return;
  }

  $url = parse_url($_SERVER['REQUEST_URI']);

  if ($url['path'] === '/') {
    // Root URL
    header('Location: /download/daily/');
  } elseif (startsWith($url['path'], '/download')) {
    // Ensure we have '/' at the end of requested URL.
    if (!endsWith($url['path'], '/')) {
      $location = getRedirectLocationFromURL($url);
      header("Location: $location");
      return;
    }
    handleDownloadRequest();
  } elseif ($url['path'] === '/dashboard') {
    handleDashboardRequest();
  } elseif ($url['path'] === '/tests') {
    handleTestsRequest();
  } else {
    // URL not found
    handleNotFoundRequest();
  }
}

?>
