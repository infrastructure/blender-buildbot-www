<?php

class BuildReleaseCycle {
  const ALPHA = 'alpha';
  const BETA = 'beta';
  const CANDIDATE = 'candidate';
  const STABLE = 'stable';
}

class Build {
  public $app;
  public $version;
  public $risk_id;
  public $branch;
  public $patch;
  public $hash;
  public $platform;
  public $architecture;
  public $build_configuration;
  public $bitness;
  public $file_mtime;
  public $file_name;
  public $file_size;
  public $file_extension;
  public $release_cycle;
  public $sha256_checksum;
  public $directory_lister;

  private function __construct(string $file_path) {
    if (!$file_path) {
      return null;
    }

    $path_parts = pathinfo($file_path);

    $file_name = $path_parts['basename'];
    $file_name_no_ext = $path_parts['filename'];
    $file_extension = $path_parts['extension'] ?? '';

    $regexp = '/^(?<app>[a-z]+)-' .
      '(?<version>[0-9]+\.[0-9]+\.[0-9]+)\-' .
      '(?<risk_id>[a-z]+)\+' .
      '(?<branch>[A-Za-z0-9_\-]+)\.' .
      '(?<hash>[a-fA-f0-9]+)\-' .
      '(?<platform>[A-Za-z0-9_]+)\.' .
      '(?<architecture>[A-Za-z0-9_]+)\-' .
      '(?<build_configuration>(release|debug))/m';

    $is_matched = preg_match($regexp, $file_name_no_ext, $matches);
    if (!$is_matched) {
      return;
    }

    // User readable name.
    $app = $matches['app'];
    if ($app == "blender") {
      $app = "Blender";
    } else if ($app == "bpy") {
      $app = "bpy module";
    } else {
      return;
    }

    // Get basic information from file name.
    $this->app = $app;
    $this->version = $matches['version'];
    $this->risk_id = $matches['risk_id'];
    $this->branch = $matches['branch'];
    $this->hash = $matches['hash'];
    $this->platform = $matches['platform'];
    $this->architecture = $matches['architecture'];
    $this->build_configuration = $matches['build_configuration'];
    $this->bitness = 64;

    // Extract patch from branch.
    if (stringContains($this->branch, "main-")) {
      $this->patch = explode("-", $this->branch)[1];
    } else if (stringContains($this->branch, "master-")) {
      $this->patch = explode("-", $this->branch)[1];
    }

    // File information.
    $this->file_mtime = filemtime($file_path);
    $this->file_name = $file_name;
    $this->file_size = filesize($file_path);
    $this->file_extension = $file_extension;

    // Check for existing .sha256 file.
    $sha256_file = $file_path . '.sha256';
    if (file_exists($sha256_file)) {
        $this->sha256_checksum = strtoupper(trim(str_replace(["\r", "\n", "\t"], '', file_get_contents($sha256_file))));
    } else {
        $this->sha256_checksum = null;
    }
    
    // Release cycle.
    //
    // NOTE: Keep it last, so the rule matcher can access all possible
    // information.
    $this->release_cycle = getBuildReleaseCycle($this);
  }

  public static function Create(string $file_path): ?Build {
    $build = new Build($file_path);
    if (!$build->file_name) {
      return null;
    }
    return $build;
  }
}

?>
