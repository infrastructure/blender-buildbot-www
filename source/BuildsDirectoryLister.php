<?php

class BuildsDirectoryLister {
  protected $path;
  protected $real_path;
  protected $is_daily;
  protected $is_experimental;
  protected $is_patch;
  protected $is_python_module;
  protected $branch;
  protected $patch;

  public function __construct(string $path) {
    $document_root = $_SERVER['DOCUMENT_ROOT'];
    $this->is_daily = stringContains($path, "daily");
    $this->is_experimental = stringContains($path, "experimental");
    $this->is_patch = stringContains($path, "patch");
    $this->is_python_module = stringContains($path, "bpy");

    if (basename($path) == "archive") {
      // No per-branch/patch archive for now to keep things simple.
    } else if ($this->is_patch) {
      $patch = basename($path);
      if ($patch != "patch") {
        $this->patch = $patch;
        $path = dirname($path);
      }
    } else if ($this->is_experimental) {
      $branch = basename($path);
      if ($branch != "experimental") {
        $this->branch = $branch;
        $path = dirname($path);
      }
    }
    else if ($this->is_python_module) {
      // Python modules are in the daily folder, not their own.
      $path = pathJoin(dirname($path), "daily");
    }

    $this->path = $this->getValidPath($path);
    if ($this->path != NULL) {
      $this->real_path = pathJoin($document_root, $this->path);
    } else {
      $this->real_path = NULL;
    }
  }

  // For a path provided at a construction time, get ordered list
  // of builds to be displayed.
  public function getBuilds(): array {
    $builds = array();
    if ($this->real_path == NULL) {
      return $builds;
    }

    if (!file_exists($this->real_path)) {
      return $builds;
    }

    $files = scandir($this->real_path);
    foreach ($files as $file_name) {
      // Ignore all hidden files.
      if ($file_name[0] == '.') {
        continue;
      }

      $file_path = pathJoin($this->real_path, $file_name);

      if (!is_file($file_path)) {
        // Ignore all directories, like the archive.
        continue;
      }

      if (startsWith($file_name, 'tmp')) {
        // Ignore temporary upload files.
        continue;
      }

      $build = Build::Create($file_path);
      if (!$build) {
        continue;
      }

      // Filter blender/bpy from same folder onto different pages.
      if ($this->is_daily && $build->app == "bpy module") {
        continue;
      }
      else if ($this->is_python_module && $build->app != "bpy module") {
        continue;
      }

      $build->directory_lister = $this;

      $builds[] = $build;
    }

    // TODO(sergey): Sort builds.
    return $builds;
  }

  // For a given requested path return path which will be listed.
  protected function getValidPath(string $path): ?string {
    // First remove any possible leading slashes.
    // They are added by redirect rule and are fine.
    $path = ltrim(ltrim($path, '/'), "\\");
    if ($path == "") {
      return NULL;
    }
    if (!$this->checkPathValid($path)) {
      return NULL;
    }
    return $path;
  }

  protected function checkPathValid(string $path): bool {
    if (!preg_match('/^[a-z_\-0-9\/]+$/i', $path)) {
      return false;
    }
    // TODO(sergey): More checks?
    return true;
  }

  public function isValid(): bool {
    $search_path = $this->path;
    if ($search_path == NULL) {
      return false;
    }

    return stringContains($search_path, "download/daily") ||
      stringContains($search_path, "download/experimental") ||
      stringContains($search_path, "download/patch") ||
      stringContains($search_path, "download/bpy");
  }

  public function exists(): bool {
    return file_exists($this->real_path);
  }

  public function isDaily(): bool {
    return ($this->is_daily);
  }

  public function isExperimental(): bool {
    return ($this->is_experimental);
  }

  public function isPatch(): bool {
    return ($this->is_patch);
  }

  public function isPythonModule(): bool {
    return ($this->is_python_module);
  }

  public function getBranch(): ?string {
    return $this->branch;
  }

  public function getPatch(): ?string {
    return $this->patch;
  }
}

?>
