<?php

// Only call this if filename of build does not contain rc
// We will fix this until we have build manifests.
function getBuildReleaseCycle($build): string {
  if ($build->risk_id == 'alpha') {
    return BuildReleaseCycle::ALPHA;
  }
  if ($build->risk_id == 'beta') {
    return BuildReleaseCycle::BETA;
  }
  if ($build->risk_id == 'candidate') {
    return BuildReleaseCycle::CANDIDATE;
  }
  if ($build->risk_id == 'stable') {
    return BuildReleaseCycle::STABLE;
  }
  // Fallback, should not happen.
  return BuildReleaseCycle::ALPHA;
}

function isVisibleBuild($build): bool {
  // if ($build->release_cycle == BuildReleaseCycle::STABLE) {
  //   return false;
  // }

  if ($build->directory_lister->isExperimental()) {
    $filter_branch = $build->directory_lister->getBranch();
    if ($filter_branch && $build->branch != $filter_branch) {
      return false;
    }
  } else if ($build->directory_lister->isPatch()) {
    $filter_patch = $build->directory_lister->getPatch();
    if ($filter_patch && $build->patch != $filter_patch) {
      return false;
    }
  }

  return true;
}

?>
