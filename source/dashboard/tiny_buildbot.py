import requests

from typing import Any, Optional


class API:
    """
    API integration level
    """

    # URL to a root of the API.
    # For example, https://builder.blender.org/admin/api/
    api_url: str

    def __init__(self, api_url) -> None:
        self.api_url = api_url.rstrip("/")

    def call(self, function: str, params: dict = {}) -> None:
        """
        Call the given API function with parameters

        For example:
          api.call(function=f"builders/{self.id}/builds",
                   params={"limit": limit, "order": order, "offset": offset})
        """

        url = self._get_request_url(function)

        response = requests.get(url, params=params)

        return response.json()

    def _get_request_url(self, function: str) -> str:
        """
        Get URL for the given function end point
        """
        return f"{self.api_url}/v2/{function}"


class Base:
    """
    Base for all buildbot classes with API support.
    """

    api: API

    def __init__(self, api: API) -> None:
        self.api = api


class Build(Base):
    """
    Buildbot build definition.
    """

    builder: "Builder"
    id: int
    number: int

    # UNIX timestamp, unknown timezone
    started_at: int
    complete_at: Optional[int]

    complete: bool
    properties: dict[Any]
    state_string: str

    def __init__(
        self,
        builder: "Builder",
        id=-1,
        number=-1,
        started_at=0,
        complete_at=0,
        complete=False,
        properties={},
        state_string="",
    ) -> None:
        super().__init__(api=builder.api)

        self.builder = builder

        self.id = id
        self.number = number
        self.started_at = started_at
        self.complete_at = complete_at
        self.complete = complete
        self.properties = properties
        self.state_string = state_string

    def __repr__(self) -> str:
        return f"Build(id={self.id}, complete={self.complete})"

    def get_url(self) -> str:
        url_prefix = self.api.api_url.rstrip("/api")
        return f"{url_prefix}/#/builders/{self.builder.id}/builds/{self.number}"

    @classmethod
    def from_json(cls, builder: "Builder", json: dict) -> "Build":
        build = Build(builder=builder)

        build.id = int(json["buildid"])
        build.number = int(json["number"])
        build.started_at = int(json["started_at"])
        build.complete_at = int(json["complete_at"]) if json["complete_at"] is not None else None
        build.complete = bool(json["complete"])
        build.properties = json["properties"]
        build.state_string = json["state_string"]

        return build


class Builder(Base):
    """
    Builbot builder definition.
    """

    id: int
    name: str
    description: str
    master_ids: list[int]

    tags: list[str]

    # TODO(sergey): master_ids
    # TODO(sergey): tags

    def __init__(self, api, id=-1, name="", description=""):
        super().__init__(api=api)

        self.id = id
        self.name = name
        self.description = description

        self.tags = name.split("-")

    def get_builds(self, limit=100, offset=0, order="-number") -> list[Build]:
        builds_json = self.api.call(
            function=f"builders/{self.id}/builds",
            params={
                "limit": limit,
                "order": order,
                "offset": offset,
                # XXX
                "property": "needs_package_delivery",
            },
        )

        builds = []

        for build_json in builds_json["builds"]:
            builds.append(Build.from_json(self, build_json))

        return builds

    @classmethod
    def from_json(cls, api: API, json: dict) -> "Builder":
        builder = Builder(api=api)

        builder.id = int(json["builderid"])
        builder.name = json["name"]
        builder.description = json["description"]
        builder.master_ids = json["masterids"]

        builder.tags = builder.name.split("-")

        return builder

    def __repr__(self) -> str:
        return f"Builder(id={self.id}, name='{self.name}')"


class Builders(Base):
    """
    A collection of builders.
    With handy utilities for searching.
    """

    builders: list[Builder]

    def __init__(self, api: API) -> None:
        super().__init__(api=api)

        self.builders = []

    def builder_with_name(self, name: str) -> Optional[Builder]:
        for builder in self.builders:
            if builder.name == name:
                return builder
        return None

    def all_sorted(self) -> list[Builder]:
        builders = sorted(self.builders, key=lambda x: x.name)
        return builders

    @classmethod
    def from_json(cls, api: API, json: dict) -> "Builders":
        builders = Builders(api=api)

        for builder_json in json["builders"]:
            builders.builders.append(Builder.from_json(api, builder_json))

        return builders

    def __repr__(self) -> str:
        return self.builders.__repr__()


class Client:
    """
    Minimalistic buildbot REST client
    """

    api: API

    def __init__(self, api_url) -> None:
        self.api = API(api_url=api_url)

    def get_builders(self) -> Builders:
        builders_json = self.api.call(function="builders")

        return Builders.from_json(self.api, builders_json)
