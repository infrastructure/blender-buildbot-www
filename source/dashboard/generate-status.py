#!/usr/bin/env python3

# Script which talks to the Blender Buildbot and generates JSON needed for the
# dashboard generation.
#
# Running this script without any arguments, ot with '-' as a single argument
# will print the json into the STDOUT. Otherwise the script expects a file name
# to be provided.
#
# A saving to a temp file followed with an os.rename() is used to make the
# update process as atomic as possible.

import argparse
import json
import dataclasses
import os

from pathlib import Path

from tiny_buildbot import Client, Builder
from builder_status import BuilderInfo

BUILDBOT_URL_PREFIX = "https://builder.blender.org/admin"
API_URL = f"{BUILDBOT_URL_PREFIX}/api/"


# Pagination limit: the number of builds which are queries as a single request
# from the buildbot.
MAX_NUM_BUILDS_PER_REQUEST = 10

# The maximum number of pages of builds to query.
MAX_NUM_BUILDS_PAGES = 10


# https://stackoverflow.com/a/51286749
class EnhancedJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        return super().default(o)


def _is_interesting_builder(builder: Builder) -> bool:
    if not builder.master_ids:
        return False

    if "coordinator" not in builder.tags:
        return False

    if "patch" in builder.tags or "vexp" in builder.tags:
        return False

    return True


def _get_json_string() -> str:
    client = Client(api_url=API_URL)
    builders = client.get_builders()

    builders_status = []

    for builder in builders.all_sorted():
        if not _is_interesting_builder(builder):
            continue
        builder_info = BuilderInfo(builder)
        builder_status = builder_info.get_status()

        builders_status.append(
            {
                "id": builder.id,
                "name": builder.name,
                "status": builder_status,
            }
        )

    builders_status = sorted(builders_status, key=lambda x: x["name"])

    global_status = {"builders": builders_status}

    return json.dumps(global_status, cls=EnhancedJSONEncoder, indent=2)


def main():
    parser = argparse.ArgumentParser(description="Buildbot Status")

    parser.add_argument(
        "output",
        nargs="?",
        type=Path,
        default="-",
        help="Output path for the json. Default is the stdout",
    )

    args = parser.parse_args()

    json_str = _get_json_string()

    if str(args.output) == "-":
        print(json_str)
        return

    temp_path = args.output.with_suffix(args.output.suffix + ".tmp")
    temp_path.parent.mkdir(parents=True, exist_ok=True)
    temp_path.write_text(json_str)

    os.rename(temp_path, args.output)


if __name__ == "__main__":
    main()
