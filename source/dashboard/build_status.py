from dataclasses import dataclass
from typing import Optional

from tiny_buildbot import Build


@dataclass
class BuildStatus:
    """
    Status information about a particular build
    """

    number: int

    # True when the build succeeded, false otherwise.
    ok: bool

    # UNIX timestamp, unknown timezone
    started_at: int
    complete_at: Optional[int]

    # URL to the build information in the buildbot interface
    url: str

    def __init__(
        self, number=-1, ok=False, started_at=0, complete_at=0, url=""
    ) -> None:
        self.number = number
        self.ok = ok
        self.started_at = started_at
        self.complete_at = complete_at
        self.url = url


class BuildInfo:
    build: Build

    def __init__(self, build: Build):
        self.build = build

    def get_status(self) -> Optional[BuildStatus]:
        if not self._status_usable():
            return None

        build = self.build

        build_status = BuildStatus(
            number=build.number,
            started_at=build.started_at,
            complete_at=build.complete_at,
            url=build.get_url(),
        )

        if self.build.state_string == "build successful":
            build_status.ok = True

        return build_status

    def is_delivery(self) -> bool:
        """
        Returns true if this is a build which does delivery
        """

        if self._needs_package_delivery():
            return True

        return False

    def is_ci(self) -> bool:
        """
        Returns true if this is a build which does continuous integration
        """

        if self._is_delivery_only_build():
            return False

        return True

    def _status_usable(self) -> bool:
        if not self.build.complete:
            return False

        return True

    def _is_delivery_only_build(self) -> bool:
        build = self.build
        builder = build.builder

        if "deploy" in builder.tags:
            return True

        if "doc" in builder.tags:
            return True

        if "store" in builder.tags:
            return True

        if "needs_package_delivery" in build.properties:
            return False

        return False

    def _needs_package_delivery(self) -> bool:
        if self._is_delivery_only_build():
            return True

        prop = self.build.properties.get("needs_package_delivery", None)
        if prop is None:
            return False

        assert isinstance(prop, list)
        assert len(prop) == 2

        return bool(prop[0])
