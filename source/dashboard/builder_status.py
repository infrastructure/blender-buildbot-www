from dataclasses import dataclass
from typing import Optional

from tiny_buildbot import Builder
from build_status import BuildStatus, BuildInfo

# Pagination limits: the number of results per page and the overall number of
# pages to check.
MAX_NUM_BUILDS_PER_REQUEST = 10
MAX_NUM_BUILDS_PAGES = 10


@dataclass
class BuilderStatus:
    """
    Overall status of a builder.
    """

    delivery: Optional[BuildStatus]
    ci: Optional[BuildStatus]

    def __init__(self, delivery=None, ci=None) -> None:
        self.delivery = delivery
        self.ci = ci

    def __repr__(self) -> str:
        return f"BuilderStatus(delivery={self.delivery}, ci={self.ci})"


class BuilderInfo:
    builder: Builder

    def __init__(self, builder: Builder):
        self.builder = builder

    def get_status(self) -> BuilderStatus:
        builder_status = BuilderStatus()

        needs_delivery_status = self.supports_delivery()
        needs_ci_status = self.supports_ci()

        offset = 0
        for i in range(MAX_NUM_BUILDS_PAGES):
            builds = self.builder.get_builds(
                limit=MAX_NUM_BUILDS_PER_REQUEST, offset=offset
            )

            if not builds:
                # No more builds left.
                break

            for build in builds:
                build_info = BuildInfo(build)

                build_status = build_info.get_status()
                if not build_status:
                    continue

                if (
                    needs_delivery_status
                    and not builder_status.delivery
                    and build_info.is_delivery()
                ):
                    builder_status.delivery = build_status

                if needs_ci_status and not builder_status.ci and build_info.is_ci():
                    builder_status.ci = build_status

                if (
                    not needs_delivery_status or builder_status.delivery is not None
                ) and (not needs_ci_status or builder_status.ci is not None):
                    return builder_status

            offset += len(builds)

        return builder_status

    def supports_delivery(self) -> bool:
        """
        Check whether the builder supports the package delivery
        """

        return True

    def supports_ci(self) -> bool:
        """
        Check whether the builder supports continuous integration
        """

        if "deploy" in self.builder.tags:
            return False

        if "doc" in self.builder.tags:
            return False

        if "store" in self.builder.tags:
            return False

        return True
