<div class="devfund">
  <div class="container py-4 px-5">
    <div class="row mt-4">
      <div class="col-lg-6 col-12 col-msg mb-4 pt-md-4 text-light">
        <h2 class="fw-bold mb-4">Donate to Blender</h2>
        <p>
          Help keep the project alive.
        </p>
        <p>
          <strong>Less than 2%</strong> of users donate to Blender.
          Your contribution will make a difference.
        </p>
        <p>
          We don't show advertisements or sell your data.<br>
          We are completely funded by donations from our community.
        </p>
      </div>
      <div class="col-lg-6 col-12">
        <div id="js-donation-box"></div>
      </div>
    </div>
  </div>
</div>
