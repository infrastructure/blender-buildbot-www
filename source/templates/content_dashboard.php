<?php
  $path = 'data/buildbot-status.json';
  $json_data = [];
  if (file_exists($path)) {
    $json_string = file_get_contents($path);
    $json_data = json_decode($json_string, true);
  } else {
    die('Could not find data/buildbot-status.json');
  }

  // Build table for delivery statuses. An example row looks like this:
  // <tr>
  //   <th scope="row">v330-code-artifacts-deploy-coordinator</th>
  //   <td><span class="badge badge-success">Success</span></td>
  //   <td>10h ago</td>
  //   <td><span class="badge badge-success">Success</span></td>
  //   <td>10h ago</td>
  // </tr>
  $table_builders = '';
  foreach ($json_data['builders'] as $key => $builder) {
    if (!isset($builder['status'])) { continue; }
    $table_builders .= '<tr>';
    $table_builders .= '<th scope="row">' . $builder['name'] . '</th>';

    // Build the status label and style
    $delivery_badge = '';
    $delivery_last_updated = '';
    if (isset($builder['status']['delivery'])) {
      $delivery_label = ($builder['status']['delivery']['ok'] == true) ? 'Success' : 'Failure';
      $delivery_class = ($builder['status']['delivery']['ok'] == true) ? 'success' : 'danger';
      $delivery_badge = '<a href="' . $builder['status']['delivery']['url'] . '">';
      $delivery_badge .= '<span class="badge badge-' . $delivery_class . '">' . $delivery_label . '</span>';
      $delivery_badge .= '</a>';
      $delivery_last_updated = (isset($builder['status']['delivery']['complete_at']) ? formatTimestamp($builder['status']['delivery']['complete_at']) : '-');
    }
    $table_builders .= '<td>' . $delivery_badge . '</td>';
    $table_builders .= '<td>' . $delivery_last_updated . '</td>';

    // Build the CI label and style
    $ci_badge = '';
    $ci_last_updated = '';
    if (isset($builder['status']['ci'])) {
      $ci_label = ($builder['status']['ci']['ok'] == true) ? 'Success' : 'Failure';
      $ci_class = ($builder['status']['ci']['ok'] == true) ? 'success' : 'danger';

      $ci_badge = '<a href="' . $builder['status']['ci']['url'] . '">';
      $ci_badge .= '<span class="badge badge-' . $ci_class . '">' . $ci_label . '</span>';
      $ci_badge .= '</a>';

      $ci_last_updated = (isset($builder['status']['ci']['complete_at']) ? formatTimestamp($builder['status']['ci']['complete_at']) : '-');
    }
    $table_builders .= '<td>' . $ci_badge . '</td>';
    $table_builders .= '<td>' . $ci_last_updated . '</td>';

    $table_builders .= '</tr>';
  }

?>

<div class="container my-4 builder-dashboard">
  <h1>Builders Status</h1>
  <table class="table table-sm my-3 monospace compact">
    <thead>
      <tr>
        <th scope="col">Builder</th>
        <th scope="col">Delivery</th>
        <th scope="col">Last Update</th>
        <th scope="col">CI</th>
        <th scope="col">Last Update</th>
      </tr>
    </thead>
    <tbody>
      <?= $table_builders ?>
    </tbody>
  </table>

</div>

