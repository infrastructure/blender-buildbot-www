<div class="container my-4 builder-dashboard">
  <h3>Daily Benchmarks</h3>
  <div>
    <div>
      Results of automated testing of various performance aspects of Blender, such as
      Blend-file open time, render time and memory usage, geometry nodes evaluation, and
      so on.
    </div>
    <div>The intention is to detect possible performance improvements or regressions.</div>
    <ul>
      <li><a href="https://builder.blender.org/download/daily/benchmarks/main-darwin-arm64/report.html">macOS report</a></li>
      <li><a href="https://builder.blender.org/download/daily/benchmarks/main-linux-x86_64/report.html">Linux report</a></li>
    </ul>
  </div>

  <h3>OpenData</h3>
  <div>
    For stable releases you can browse and submit benchmarks using the <a href="https://opendata.blender.org/">opendata platform</a>.
  </div>
</div>

