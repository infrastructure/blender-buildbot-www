<!DOCTYPE html>
<html data-theme="dark" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php
      $social_title = 'Blender Builds - blender.org';
      $social_text = 'Continuous build delivery with the latest features and fixes. Use at your own risk!';
      $social_permalink = 'https://builder.blender.org/';
      $social_image = 'https://builder.blender.org/assets/images/blender_creative_freedom_red.jpg';
    ?>
    <title><?=$social_title?></title>
    <meta name="author" content="Blender Foundation">
    <meta name="description" content="<?=$social_text?>">
    <meta name="theme-color" content="#ed8f31">
    <meta property="og:site_name" content="<?=$social_title?>" >
    <meta property="og:title" content="<?=$social_title?>" >
    <meta property="og:description" content="<?=$social_text?>">
    <meta property="og:url" content="<?=$social_permalink?>">
    <meta property="og:image" content="<?=$social_image?>">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@blender">
    <meta name="twitter:title" content="<?=$social_title?>">
    <meta name="twitter:description" content="<?=$social_text?>">
    <meta name="twitter:image" content="<?=$social_image?>">

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/icons/apple-touch-icon.png">
    <link rel="icon" type="image/svg+xml" href="/assets/icons/favicon.svg">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/icons/favicon-16x16.png">

    <!-- TODO: add web-assets v2 upgrade markup and style changes -->
    <link href="/assets/css/main.css?v=2.0.0-alpha.30.1" rel="stylesheet">
    <script defer data-domain="builder.blender.org" src="https://analytics.blender.org/js/script.tagged-events.outbound-links.file-downloads.js"></script>
    <script>window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }</script>

    <noscript>
      <style>
        /* If JavaScript is disabled, show all builds. */
        .builds-list {
          border-radius: var(--border-radius-lg);
          display: block;
          opacity: 1;
          visibility: visible;
        }
        /* Hide platform header and empty lists. */
        .platforms-list-header,
        .builds-list .builds-list-empty {
          display: none;
        }

        /* Show the name of the platform, since the header is hidden. */
        .build-platform {
          display: inline-block;
        }
      </style>
    </noscript>
  </head>

  <body>

    <?php include('navbar_global.php'); ?>

    <nav class="navbar navbar-secondary">
      <div class="container">
        <ul class="navbar-nav">
          <?php
            $current_path = parse_url($_SERVER['REQUEST_URI'])['path'];
            $current_page_class = 'class="current_page_item"';
          ?>
          <li <?=($current_path == '/download/daily/' ? $current_page_class : '')?>>
            <a href="/download/daily/" class="nav-link">Daily</a>
          </li>
          <li <?=($current_path == '/download/experimental/' ? $current_page_class : '')?>>
            <a href="/download/experimental/" class="nav-link">Branch</a>
          </li>
          <li <?=($current_path == '/download/patch/' ? $current_page_class : '')?>>
            <a href="/download/patch/" class="nav-link">Patch</a>
          </li>
          <li <?=($current_path == '/download/bpy/' ? $current_page_class : '')?>>
            <a href="/download/bpy/" class="nav-link">Python Module</a>
          </li>
          <li <?=($current_path == '/dashboard' ? $current_page_class : '')?>>
            <a href="/dashboard" class="nav-link">Dashboard</a>
          </li>
          <li <?=($current_path == '/tests' ? $current_page_class : '')?>>
            <a href="/tests" class="nav-link">Tests</a>
          </li>
          <li>
            <a href="/admin/" class="nav-link">Buildbot</a>
          </li>
          <li>
            <a href="https://developer.blender.org/docs/release_notes/" target="_blank" class="nav-link">Latest Changes</a>
          </li>
        </ul>
      </div>
    </nav>



