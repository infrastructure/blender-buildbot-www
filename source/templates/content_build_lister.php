<div class="hero-helper">
  <div class="hero">
    <div class="container">
      <div class="hero-content">
        <?php if ($lister->isExperimental()) { ?>
          <?php
            if ($lister->getBranch()) {
              echo "<h1>" . $lister->getBranch() . " Builds</h1>";
            } else {
              echo "<h1>Branch Builds</h1>";
            }
          ?>

          <div class="hero-subtitle">
            <p>The following builds are from official branches.
              They have new features that may end up in official Blender releases.
              They can be unstable and break your files.
              <strong>Use at your own risk.</strong>
            </p>
          </div>

          <?php } elseif ($lister->isPatch()) { ?>

          <?php
            if ($lister->getPatch()) {
              echo "<h1>" . $lister->getPatch() . " Builds</h1>";
            } else {
              echo "<h1>Patch Builds</h1>";
            }
          ?>

          <div class="hero-subtitle">
            <p>The following builds have Patch based builds.
              They can be unstable and break your files.
              <strong>Use at your own risk.</strong>
            </p>
          </div>

          <?php } elseif ($lister->isPythonModule()) { ?>

          <h1>Python Module Builds</h1>
          <div class="hero-subtitle">
            <p>Builds of Blender as a Python module.</p>
          </div>

        <?php } else { ?>
          <h1>Daily Builds</h1>
          <div class="hero-subtitle">
            <p>The following builds have the latest features and bug fixes,
               but they can be unstable and break your files.
              <strong>Use at your own risk.</strong>
            </p>
          </div>
        <?php } ?>

        <div class="w-100">
          <div class="platforms-list-header">
            <span class="platform-title js-toggle-content" data-platform="windows">
              <i class="i-windows"></i> Windows
            </span>
            <span class="platform-title js-toggle-content" data-platform="darwin">
              <i class="i-macos"></i> macOS
            </span>
            <span class="platform-title js-toggle-content" data-platform="linux">
              <i class="i-linux"></i> Linux
            </span>
            <div class="platform-filter">
              <select id="platform-filter">
                <option value='all' selected>All Architectures</option>
                <option value='is-x64-only' data-label-darwin="Intel" data-label-windows="x64" data-label-linux="x64">
                  x64
                </option>
                <option value='is-arm64-only' data-label-darwin="Apple Silicon" data-label-windows="arm64" data-label-linux="arm64">
                  arm64
                </option>
              </select>
            </div>
          </div>

          <div id="builds-container">
            <?php
              $builds = $lister->getBuilds();
              $renderer = new BuildsRenderer($builds);
              $renderer->renderPage();
            ?>
          </div>

          <?php /* Link to archive under the list of builds, if page is not archive. */ ?>
          <?php
            $pagePath = $_SERVER['REQUEST_URI'];

            // Check if page is not archive
            if (!(substr($pagePath, -8) == "archive/")):
            ?>
            <div class="builds-archive">
              <?php if ($lister->isExperimental()) { ?>
                <?php if ($lister->getBranch()) { ?>
                  <a href="/download/experimental/">All Branches</a> |
                <?php } ?>
                <a href="/download/experimental/archive/">All Archived Builds</a>
              <?php } elseif ($lister->isPatch()) { ?>
                <?php if ($lister->getPatch()) { ?>
                  <a href="/download/patch/">All Patches</a> |
                <?php } ?>
                <a href="/download/patch/archive/">All Archived Builds</a>
              <?php } elseif ($lister->isPythonModule()) { ?>
                <a href="/download/bpy/archive/">All Archived Builds</a>
              <?php } else { ?>
                <a href="/download/daily/archive/">All Archived Builds</a>
              <?php } ?>
            </div>
            <?php
          endif; ?>
        </div>
      </div>
    </div>
    <div class="hero-bg" style="background-image: url(https://www.blender.org/wp-content/uploads/2018/11/gleb-alexandrov-exploding-madness.jpg); background-position-y: 0%">
    </div>
    <div class="hero-overlay"></div>
    <div class="hero-overlay-bottom"></div>
  </div>
</div>

<?php include('banner_devfund.php'); ?>

  <script>
    document.addEventListener('DOMContentLoaded', () => {
      const platformFilter = document.getElementById('platform-filter');
      const container = document.getElementById('builds-container');

      /* Filter list by adding the select value as class to the builds container. */
      const filterBuildsList = (selectedValue) => {
        container.className = selectedValue;
      };

      /* When selecting an option, save it on localStorage. */
      platformFilter.addEventListener('change', (event) => {
        const selectedValue = event.target.value;
        localStorage.setItem('platformFilter', selectedValue);
        filterBuildsList(selectedValue);
      });

      /* If an option was selected before, filter by that. */
      const savedValue = localStorage.getItem('platformFilter') || 'all';
      platformFilter.value = savedValue;
      filterBuildsList(savedValue);
    });
  </script>
