<?php

function createListerForPath($path): BuildsDirectoryLister {
  return new BuildsDirectoryLister($path);
}

function createListerForCurrentRequest(): BuildsDirectoryLister {
  # This key is a rewrite set via Apache site config
  $path = $_GET['__path__'] ?? '';
  return createListerForPath($path);
}

function specialActionInfo(): bool {
  $lister = createListerForCurrentRequest();

  if (!$lister->isValid()) {
    return false;
  }

  $builds = $lister->getBuilds();
  header('Content-Type: text/plain');
  print(json_encode($builds));
  return true;
}

function handleSpecialAction(): bool {
  if (!array_key_exists('action', $_GET)) {
    return false;
  }

  $action = $_GET['action'];
  if ($action == 'info') {
    return specialActionInfo();
  }

  return false;
}

?>
