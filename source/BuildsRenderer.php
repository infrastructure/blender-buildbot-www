<?php


class BuildsRenderer {
  protected $builds;

  public function __construct($builds) {
    $this->builds = $builds;
  }

  public function renderJSON($version = 1) {
    /* Return a JSON formatted list of builds. */
    $builds_json = array();
    foreach ($this->builds as $build) {
      $build_data = array(
        'url' => $this->getFileNameURL($build),
        'app' => $build->app,
        'version' => $build->version,
        'risk_id' => $build->risk_id,
        'branch' => $build->branch,
        'patch' => $build->patch,
        'hash' => $build->hash,
        'platform' => $build->platform,
        'architecture' => $build->architecture,
        'bitness' => $build->bitness,
        'file_mtime' => $build->file_mtime,
        'file_name' => $build->file_name,
        'file_size' => $build->file_size,
        'file_extension' => $build->file_extension,
        'release_cycle' => $build->release_cycle,
      );

      if ($version == 2) {
        // If version 2 is requested, skip any .sha256 files and add a checksum key-value entry
        if (endsWith($build->file_name, '.sha256')) {
          continue;
        }
        $build_data['checksum'] = $build->sha256_checksum;
      }

      $builds_json[] = $build_data;
    }
    echo(json_encode($builds_json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
  }

  public function renderPage() {
    $platform_filters = array(
        'windows',
        'linux',
        'darwin'
      );

    foreach ($platform_filters as $platform) {
      $builds = $this->getBuildsForPlatform($platform);
      $this->sortBuilds($builds);
      /* Build the list of builds. */
      print('<div class="builds-list-container" data-platform="' . $platform . '">');

      $this->renderBuildsTable($builds);
      print('</div>');
    }
  }

  protected function getBuildsForPlatform(string $platform): array {
    $builds = array();
    foreach ($this->builds as $build) {
      if ($build->platform == $platform) {
        $builds[] = $build;
      }
    }
    return $builds;
  }

  protected static function buildComparator($a, $b): int {
    // Keep major versions grouped.
    if ($a->version < $b->version) {
      return -1;
    } else if ($a->version > $b->version) {
      return 1;
    }

    // Then sort by time ascending.
    if ($a->file_mtime < $b->file_mtime) {
      return -1;
    } else if ($a->file_mtime > $b->file_mtime) {
      return 1;
    }

    return 0;
  }

  protected function sortBuilds(array& $builds) {
    usort($builds, array($this, 'buildComparator'));
  }

  protected function constructBuildHashLink(Build $build): string {
    if ($build->directory_lister->isPatch()) {
      $patch_id = explode("-", $build->branch)[1];
      if (!startsWith($patch_id, 'PR')) {
        return '';
      }
      $pr_string = substr($patch_id, 2);

      return '<a class="t-cell b-reference" href="https://projects.blender.org/blender/blender/pulls/' . $pr_string . '" title="See pull request" target="_blank">!' . $pr_string . '</a>';
    }
    return '<a class="t-cell b-reference" href="https://projects.blender.org/blender/blender/commit/' . $build->hash . '" title="See commit" target="_blank">' . $build->hash . '</a>';
  }

  protected function renderBuildsTable(array $builds) {

    $builds = array_reverse($builds);

    print('<ul class="builds-list">');

    if (empty($builds)) {
      print('<li class="builds-list-empty">No builds available.</li>');
    } else {
      $table_header  = '<div class="t-row t-header">';
      $table_header .= '<span class="t-header-cell b-version">Version</span>';
      $table_header .= '<span class="t-header-cell b-variant">Variant</span>';
      $table_header .= '<span class="t-header-cell b-reference">Reference</span>';
      $table_header .= '<span class="t-header-cell b-sha">SHA</span>';
      $table_header .= '<span class="t-header-cell b-date">Date</span>';
      $table_header .= '<span class="t-header-cell b-arch">Architecture</span>';
      $table_header .= '<span class="t-header-cell b-down"></span>';
      $table_header .= '</div>';
      print($table_header);
    }

    foreach ($builds as $build) {
      $build_configuration = $build->build_configuration;
      $branch = $build->branch;
      $patch = $build->patch;

      if (!isVisibleBuild($build)) {
        continue;
      }

      $browser_platform = $this->getBrowserPlatform($build);
      $file_name_url = $this->getFileNameURL($build);
      $build_visible_name = $this->getVisibleName($build);
      $time_code = $this->getFileTimeCode($build);
      $time_code_iso = $this->getFileTimeCodeISO($build);
      $file_extension = $this->getFileExtension($build);
      $build_architecture = $this->getArchitecture($build);
      $sha256_url = $file_name_url . '.sha256';
      $build_date = '<div class="t-cell b-date" title="' . $time_code_iso . '">' . $time_code . '</div>';
      $build_hash_link = $this->constructBuildHashLink($build);
      $build_size = $this->getFileSizeCode($build);

      $is_patch = $build->directory_lister->isPatch();
      $is_experimental = $build->directory_lister->isExperimental();

      $build_variant_name = $this->getBuildVariantStyleName($build);
      if ($is_patch) {
        $build_variant_name = explode("-", $build->branch)[1];
      } elseif ($is_experimental) {
        $build_variant_name = $build->branch;
      }

      /* Analytics.
      * Must match the goals set in analytics.blender.org */
      $analytics_event_name = 'Downloads+Blender';
      $analytics_event_build = $build_variant_name;
      $analytics_event_os = $browser_platform;
      $analytics_classes = 'plausible-event-name=' . $analytics_event_name . ' plausible-event-os=' . $analytics_event_os . ' plausible-event-build=' . $analytics_event_build;

      /* Hide certain items from the UI. */
      $is_release_stable = BuildReleaseCycle::STABLE == $build->release_cycle;
      $needs_hidden = in_array($file_extension, array("msi", "msix", "sha256"));
      if (!$is_patch && !$is_experimental) {
        $needs_hidden |= $is_release_stable;
      }

      $display_style = "";
      if ($needs_hidden) {
        $display_style = 'style="display:none;"';
      }

      /* Markup starts here for each build in a row. */
      print('<li class="t-row build is-' . $build->platform . ' is-arch-' . $build->architecture . '" ' . $display_style . '>');

      /* Version and variant. */
      print($build_visible_name);

      /* "All" link for all builds of a specific variant. */
      if ($build->directory_lister->isExperimental()) {
        print(' <a href="' . ($build->directory_lister->getBranch() ? "" : $branch) . '" class="b-all-builds-of-type" title="All builds of this type">all</a>');
      } else if ($build->directory_lister->isPatch()) {
        print(' <a href="' . ($build->directory_lister->getPatch() ? "" : $patch) . '" class="b-all-builds-of-type" title="All builds of this type">all</a>');
      }

      /* Reference (hash or PR). */
      print($build_hash_link);

      /* SHA-256 link. */
      print('<a href="' . $sha256_url . '" target="_blank" class="t-cell b-sha" title="SHA-256 for this file">SHA</a>');

      /* Date. */
      print($build_date);

      /* Architecture. */
      print('<div class="t-cell b-arch build-architecture" title="Architecture">');
      print('<span class="build-platform">' . $browser_platform. '</span> ');
      print($build_architecture);
      if ($build_configuration != "release") {
        print(' <span class="configuration" title="Configuration">' . $build_configuration . '</span>');
      }
      print('</div>');

      /* Download link. */
      print('<div class="t-cell b-down">');
      print('<a class="' . $analytics_classes . '" href="' . $file_name_url . '" title="Download (' . $build_size . ')">');
      print('<i class="i-download"></i>Download');
      print('</a>');
      print('</div>');

      print('</li>');
    }

    print('</ul>');
  }

  protected function getBuildVariantString($build): string {
    switch ($build->release_cycle) {
      case BuildReleaseCycle::ALPHA: return 'Alpha';
      case BuildReleaseCycle::BETA: return 'Beta';
      case BuildReleaseCycle::STABLE: return 'Stable';
      case BuildReleaseCycle::CANDIDATE: return 'Release Candidate';
    }
    // Fallback, should not happen.
    return 'Alpha';
  }

  protected function getBuildVariantStyleName($build): string {
    switch ($build->release_cycle) {
      case BuildReleaseCycle::ALPHA: return 'alpha';
      case BuildReleaseCycle::BETA: return 'beta';
      case BuildReleaseCycle::CANDIDATE: return 'candidate';
      case BuildReleaseCycle::STABLE: return 'stable';
    }
    // Fallback, should not happen.
    return 'alpha';
  }

  protected function getVisibleName($build): string {
    $build_app = $build->app;
    $build_version = $build->version;
    $build_variant_name = $this->getBuildVariantString($build);
    $build_variant_style_name = $this->getBuildVariantStyleName($build);
    $file_name_url = $this->getFileNameURL($build);

    $analytics_event_name = 'Downloads+Blender';
    $analytics_event_os = $this->getBrowserPlatform($build);
    $analytics_event_build = $build_variant_style_name;
    $analytics_classes = 'plausible-event-name=' . $analytics_event_name . ' plausible-event-os=' . $analytics_event_os . ' plausible-event-build=' . $analytics_event_build;

    if ($build->directory_lister->isPatch()) {
      $build_variant_name = explode("-", $build->branch)[1];
    } elseif ($build->directory_lister->isExperimental()) {
      $build_variant_name = $build->branch;
    }

    $build_visible_name = '<a href="' . $file_name_url . '" class="t-cell b-version ' . $analytics_classes . '">' . $build_app . ' ' . $build_version . '</a>';
    $build_visible_name .= '<a href="' . $file_name_url . '" class="t-cell b-variant ' . $analytics_classes . ' ' . $build_variant_style_name . '">' . $build_variant_name . '</a>';

    return $build_visible_name;
  }

  protected function getArchitecture($build) {
    $architecture = str_replace("86_", "", $build->architecture);
    $architecture = str_replace("amd", "x", $architecture);
    if ($build->platform == "darwin") {
      $architecture = str_replace("x64", "Intel", $architecture);
      $architecture = str_replace("arm64", "Apple Silicon", $architecture);
    }

    return $architecture;
  }

  // Web browser platform id.
  protected function getBrowserPlatform($build) {
    $build_platform = $build->platform;
    if ($build_platform == "darwin") {
      $build_platform = "macos";
    }
    return $build_platform;
  }

  protected function getFileNameURL($build): string {

    $request_uri = ($_SERVER['REQUEST_URI'] == "/" ||
                    stringContains($_SERVER['REQUEST_URI'], "bpy")) ?
      "/download/daily" : strtok($_SERVER['REQUEST_URI'], '?');

    $protocol = "https";

    $env_id = getEnvId();
    if (! in_array($env_id, array("UATEST", "PROD"))) {
      $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
    }

    // Use specific host provided for file downloads if present, otherwise serve
    // files from the same domain as the website itself.
    $download_host = getenv('DOWNLOAD_FILE_DOMAIN') ?: getHostId();
    $uri = $protocol . "://$download_host" . "$request_uri";

    $escaped_file_name = htmlspecialchars($build->file_name);

    $site_section = basename($uri);

    if (($build->directory_lister->isPatch() &&
        (($site_section != "patch") && ($site_section != "archive"))) ||
        ($build->directory_lister->isExperimental() &&
        (($site_section != "experimental") && ($site_section != "archive")))) {
      // Remove the patch/branch name from the filename.
      $uri = dirname($uri);
    }

    return pathJoin($uri, $escaped_file_name);
  }

  protected function getFileSizeCode($build): string {
    return humanFileSize($build->file_size);
  }

  protected function getFileTimeCode($build) {
    return date('d M H:i', $build->file_mtime);
  }

  protected function getFileTimeCodeISO($build) {
    return date('c', $build->file_mtime);
  }

  protected function getFileExtension($build) {
    if ($build->file_extension == "xz") {
      // HACK for now.
      return "tar.xz";
    } else {
      return $build->file_extension;
    }
  }
}

?>
