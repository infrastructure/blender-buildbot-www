<?php

function pathJoin(string $path, string $file_name): string {
  $result = $path;
  if ($path[strlen($path) - 1] != DIRECTORY_SEPARATOR) {
    $result .= DIRECTORY_SEPARATOR;
  }
  $result .= $file_name;
  return $result;
}

function startsWith(string $haystack, string $needle): bool {
    return strncmp($haystack, $needle, strlen($needle)) == 0;
}

function endsWith(string $haystack, string $needle): bool {
    return $needle == '' ||
           substr_compare($haystack, $needle, -strlen($needle)) == 0;
}

// Convert given size in bytes ot more human-readable.
function humanFileSize($bytes, $decimals = 2): string {
  $size = array('B','KB','MB','GB','TB','PB','EB','ZB','YB');
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}

function getHostId(): string {
  return $_SERVER["HTTP_HOST"] ?? 'localhost';
}

function getEnvId(): string {
  $host_id = getHostId();
  $env_id = 'LOCAL';

  if (preg_match('/\b\.blender.org\b/', $host_id)) {
    if (preg_match('/\b\.uatest\b/', $host_id)) {
      $env_id = "UATEST";
    }
    else
    {
      $env_id = "PROD";
    }
  } elseif (preg_match('/\bpvet-\b/', $host_id)) {
    $env_id = "DEVTEST";
  }

  return $env_id;
}

function stringContains(string $haystack, string $needle): bool {
  return strpos($haystack, $needle) !== FALSE;
}

// Format timestamp as "10 min ago" or similar
function formatTimestamp($timestamp): string {
  $currentTime = time();
  $diff = $currentTime - $timestamp;

  if ($diff < 60) {
      return $diff . ' sec ago';
  } elseif ($diff < 3600) {
      $minutes = floor($diff / 60);
      return $minutes . ' min ago';
  } elseif ($diff < 86400) {
      $hours = floor($diff / 3600);
      return $hours . 'h ago';
  } else {
      $days = floor($diff / 86400);
      return $days . 'd ago';
  }
}

?>
